



<?php $__env->startSection('content'); ?>

<div class="row">

	<div class="col-lg-12">

		<h1 class="page-header">

			Question Listing

		</h1>

		<a href="<?php echo e(route('questionAdd')); ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> Add</a>

		<ol class="breadcrumb">

			<li class="active">

				<i class="fa fa-dashboard"></i> Questions

			</li>

			

		</ol>

	</div>

</div>

    <div class="row justify-content-center">

        <div class="col-md-12">

			 <table id="myTable" class="table table-striped table-bordered" style="width:100%">

        <thead>

            <tr>

                <th>Id</th>

                <th>Title</th>

                <th>Type</th>

                <th>Created</th>

                <th>Action</th>

            </tr>

        </thead>

        <tbody>

        	<?php if($allQuestions): ?>

        		<?php $__currentLoopData = $allQuestions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $qt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <tr>

                <td><?php echo e($qt->id); ?></td>

                <td><?php echo e($qt->title); ?></td>

                <td>

                	<?php switch($qt->type):

					    case ('t'): ?>

					        <span>Text</span>

					        <?php break; ?>



					    <?php case ('mc'): ?>

					        <span>Multiple Choice</span>

					        <?php break; ?>

						<?php case ('sc'): ?>

					        <span>Single Choice</span>

					        <?php break; ?>

					    <?php case ('r'): ?>

					        <span>Range</span>

					        <?php break; ?>



					    <?php default: ?>

					        <span>Content Page</span>

					<?php endswitch; ?>

                </td>

                <td><?php echo e($qt->created_at); ?></td>

                <td>

                	<a class="btn btn-primary" href="<?php echo e(route('questionEdit',[$qt->id])); ?>"><i class="fa fa-edit"></i> Edit</a> &nbsp;

                	<a class="btn btn-danger" href="javascript:void(0);" data-url="<?php echo e(route('deleteQuestion',[$qt->id])); ?>" onclick="deleteQuestion(this);"><i class="fa fa-trash"></i> Delete</a>

                </td>

            </tr>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <?php endif; ?>

        </tbody>

        <tfoot>

            <tr>

                <th>Id</th>

                <th>Title</th>

                <th>Type</th>

                <th>Created</th>

                <th>Action</th>

            </tr>

        </tfoot>

    </table>

		</div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('myjsfile'); ?>

<script>

	$(document).ready(function() {

   		$('#myTable').DataTable();

	});

</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app-after-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>