<?php $__env->startSection('content'); ?>

<div class="row">

	<div class="col-lg-12">

		<h1 class="page-header">

			Edit Question

		</h1>

		<ol class="breadcrumb">

			<li class="active">

				<i class="fa fa-dashboard"></i> Question/Edit Question

			</li>

			

		</ol>

	</div>

</div>

<div class="row">

    <div class="col-md-6">

		<form action="<?php echo e(route('questionUpdate',[$questionData->id])); ?>" method="post" id="question_edit_form">

		  <?php echo csrf_field(); ?>

		  <div class="form-group contentTypeQueston">

			<label for="title">Title:</label>

			<input type="text" class="form-control" name="title" id="title"

			value="<?php echo e($questionData->title); ?>">

		  </div>

		  <div class="form-group contentTypeQueston">

			<label for="title">SubTitle:</label>

			<input type="text" class="form-control required" name="subtitle" id="subtitle" value="<?php echo e($questionData->subtitle); ?>">

		  </div>

		  <div class="form-group">

				<label for="type">Type:</label>

					<select class="form-control" name="type" size="0" onChange="selectQuestionType(this);" id="questionTypeId">

						<option value="">Select</option>

						<option value="t" <?php echo e($questionData->type == 't' ? 'selected="selected"' : ''); ?>>Text</option>

						<option value="mc" <?php echo e($questionData->type == 'mc' ? 'selected="selected"' : ''); ?>>Multiple Choice</option>

						<option value="sc" <?php echo e($questionData->type == 'sc' ? 'selected="selected"' : ''); ?>>Single Choice</option>

						<option value="r" <?php echo e($questionData->type == 'r' ? 'selected="selected"' : ''); ?>>Range</option>

						<option value="cq" <?php echo e($questionData->type == 'cq' ? 'selected="selected"' : ''); ?>>Content Page</option>

					</select>

			</div>

			<div id="SingleChoiceOptions" class="questionChoiceOptions">
				<?php if($questionData->options): ?>
					<?php $__currentLoopData = $questionData->options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$opt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="form-group <?php if($key>1): ?> <?php echo e('removeRadioBox'); ?> <?php endif; ?>">

							<label for="options<?php echo e($key+1); ?>">Option<?php echo e($key+1); ?>:</label>
							<?php if($key>1): ?>
								<div class="input-group margin-bottom-sm pull-right">
							<?php endif; ?>
							<input type="text" name="options[]" class="form-control radio-box-input"  id="options<?php echo e($key+1); ?>" value="<?php echo e((isset($questionData->options[$key]) && $questionData->type=='sc')?$questionData->options[$key]->value:''); ?>">
							<?php if($key>1): ?>
							<span class="input-group-addon" onclick="removeRadio(this);"><i class="fa fa-minus-circle"></i></span></div>
							<?php endif; ?>
						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<button type="button" class="btn btn-info pull-right" id="addMoreRadio"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add More</button>
				<?php endif; ?>

			</div>

			<div id="multipleChoiceOptions" class="questionChoiceOptions">
				<?php if($questionData->options): ?>
					<?php $__currentLoopData = $questionData->options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$opt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="form-group <?php if($key>1): ?> <?php echo e('removeCheckBox'); ?> <?php endif; ?>">

							<label for="options<?php echo e($key+1); ?>">Option<?php echo e($key+1); ?>:</label>
							<?php if($key>1): ?>
								<div class="input-group margin-bottom-sm pull-right">
							<?php endif; ?>
							<input type="text" name="options[]" class="form-control check-box-input"  id="options<?php echo e($key+1); ?>" value="<?php echo e((isset($questionData->options[$key]) && $questionData->type=='mc')?$questionData->options[$key]->value:''); ?>">
							<?php if($key>1): ?>
							<span class="input-group-addon" onclick="removeCheckBox(this);"><i class="fa fa-minus-circle"></i></span></div>
							<?php endif; ?>
						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<button type="button" class="btn btn-info pull-right" id="addMoreCheckbox"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add More</button>
				<?php endif; ?>

			</div>

			<div id="rangeChoiceOptions" class="questionChoiceOptions">

				<div class="form-group">

					<label for="options6">Start:</label>

					<input type="text" name="options[]" class="form-control"  id="options6" value="<?php echo e((isset($questionData->options[0]) && $questionData->type=='r')?$questionData->options[0]->value:''); ?>">

				</div>

				<div class="form-group">

					<label for="options7">End:</label>

					<input type="text" name="options[]" class="form-control" id="options7" value="<?php echo e((isset($questionData->options[1]) && $questionData->type=='r')?$questionData->options[1]->value:''); ?>">

				</div>

			</div>
			<div id="contentQuestionOptions" class="questionChoiceOptions">
				<div class="form-group">
					<textarea name="content_question"><?php echo e($questionData->content); ?></textarea>
				</div>
			</div>

			  <button type="submit" class="btn btn-primary">Update</button>

		</form> 

	</div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('myjsfile'); ?>
<script type="text/javascript">
	
	$(document).ready(function(){

		var ck = CKEDITOR.replace('content_question');
		ck.on( 'instanceReady', function( ev ) {
     		var editor = ev.editor;
     		editor.setReadOnly( false );
		});
		
		$('#addMoreRadio').click(function(){
			var length=$('.radio-box-input').length;
			length=parseInt(length)+1;
			var radioHtml=getRadioHtml(length);
			$(radioHtml).insertBefore(this);
		});

		$('#addMoreCheckbox').click(function(){
			var length=$('.check-box-input').length;
			length=parseInt(length)+1;
			var checkboxHtml=getCheckboxHtml(length);
			$(checkboxHtml).insertBefore(this);
		});
	});

	function getRadioHtml(index){
		return '<div class="form-group removeRadioBox"><label for="options'+index+'">Option'+index+':</label><div class="input-group margin-bottom-sm pull-right"><input type="text" name="options[]" class="form-control radio-box-input" id="options'+index+'"><span class="input-group-addon" onclick="removeRadio(this);"><i class="fa fa-minus-circle"></i></span></div></div>';
	}

	function getCheckboxHtml(index){
		return '<div class="form-group removeCheckBox"><label for="options'+index+'">Option'+index+':</label><div class="input-group margin-bottom-sm pull-right"><input type="text" name="options[]" class="form-control check-box-input" id="options'+index+'"><span class="input-group-addon" onclick="removeCheckBox(this);"><i class="fa fa-minus-circle"></i></span></div></div>';
	}

	function removeRadio(th){
		$(th).parents('.removeRadioBox').remove();
	}

	function removeCheckBox(th){
		$(th).parents('.removeCheckBox').remove();
	}

	
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app-after-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>