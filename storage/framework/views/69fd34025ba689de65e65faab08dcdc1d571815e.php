<?php $__env->startSection('content'); ?>

<div class="row">

	<div class="col-lg-12">

		<h1 class="page-header">

			Add Question

		</h1>

		<ol class="breadcrumb">

			<li class="active">

				<i class="fa fa-dashboard"></i> Question/Add Question

			</li>

			

		</ol>

	</div>

</div>

<div class="row">

	<div class="col-md-6">

		<form action="<?php echo e(route('questionSaved')); ?>" method="post" class="form-horizontal" id="question_add_form">

		  <?php echo csrf_field(); ?>

		  <div class="form-group contentTypeQueston">

			<label for="title">Title:</label>

			<input type="text" class="form-control required" name="title" id="title">

		  </div>
		  <div class="form-group contentTypeQueston">

			<label for="title">SubTitle:</label>

			<input type="text" class="form-control" name="subtitle" id="subtitle">

		  </div>

			<div class="form-group">

				<label for="type">Type:</label>

					<select class="form-control required" name="type" size="0" onChange="selectQuestionType(this);">

						<option value="">Select</option>

						<option value="t">Text</option>

						<option value="mc">Multiple Choice</option>

						<option value="sc">Single Choice</option>

						<option value="r">Range</option>

						<option value="cq">Content Page</option>

					</select>

			</div>

			<div id="SingleChoiceOptions" class="questionChoiceOptions">

				<div class="form-group">

					<label for="options1">Option1:</label>

					<input type="text" name="options[]" class="form-control radio-box-input" id="options1">

				</div>

				<div class="form-group">

					<label for="options2">Option2:</label>

					<input type="text" name="options[]" class="form-control radio-box-input" id="options2">

				</div>
				<button type="button" class="btn btn-info pull-right" id="addMoreRadio"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add More</button>

			</div>

			<div id="multipleChoiceOptions" class="questionChoiceOptions">

				<div class="form-group">
 
					<label for="options3">Option1:</label>

					<input type="text" name="options[]" class="form-control check-box-input"  id="options3">

				</div>

				<div class="form-group">

					<label for="options4">Option2:</label>

					<input type="text" name="options[]" class="form-control check-box-input" id="options4">

				</div>

				<button type="button" class="btn btn-info pull-right" id="addMoreCheckbox"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add More</button>

			</div>

			<div id="rangeChoiceOptions" class="questionChoiceOptions">

				<div class="form-group">

					<label for="options6">Start:</label>

					<input type="text" name="options[]" class="form-control"  id="options6">

				</div>

				<div class="form-group">

					<label for="options7">End:</label>

					<input type="text" name="options[]" class="form-control" id="options7">

				</div>

			</div>

			<div id="contentQuestionOptions" class="questionChoiceOptions">
				<div class="form-group">
					<textarea name="content_question"></textarea>
				</div>
			</div>

			<button type="submit" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>

			Submit</button>

		</form> 

	</div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('myjsfile'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		
		var ck = CKEDITOR.replace('content_question');
		ck.on( 'instanceReady', function( ev ) {
     		var editor = ev.editor;
     		editor.setReadOnly( false );
		});

		$('#addMoreRadio').click(function(){
			var length=$('.radio-box-input').length;
			length=parseInt(length)+1;
			var radioHtml=getRadioHtml(length);
			$(radioHtml).insertBefore(this);
		});

		$('#addMoreCheckbox').click(function(){
			var length=$('.check-box-input').length;
			length=parseInt(length)+1;
			var checkboxHtml=getCheckboxHtml(length);
			$(checkboxHtml).insertBefore(this);
		});
	});

	function getRadioHtml(index){
		return '<div class="form-group removeRadioBox"><label for="options'+index+'">Option'+index+':</label><div class="input-group margin-bottom-sm pull-right"><input type="text" name="options[]" class="form-control radio-box-input" id="options'+index+'"><span class="input-group-addon" onclick="removeRadio(this);"><i class="fa fa-minus-circle"></i></span></div></div>';
	}

	function getCheckboxHtml(index){
		return '<div class="form-group removeCheckBox"><label for="options'+index+'">Option'+index+':</label><div class="input-group margin-bottom-sm pull-right"><input type="text" name="options[]" class="form-control check-box-input" id="options'+index+'"><span class="input-group-addon" onclick="removeCheckBox(this);"><i class="fa fa-minus-circle"></i></span></div></div>';
	}

	function removeRadio(th){
		$(th).parents('.removeRadioBox').remove();
	}

	function removeCheckBox(th){
		$(th).parents('.removeCheckBox').remove();
	}

	
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app-after-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>