<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			 <h2 class="text-center">Edit Survey</h2>
			 <form action="<?php echo e(route('surveyUpdate',[$survey->id])); ?>" method="post">
			  <?php echo csrf_field(); ?>
			  <div class="form-group">
				<label for="name">Name:</label>
				<input type="text" class="form-control" name="name" id="name" value="<?php echo e($survey->name); ?>">
			  </div>
			  <div class="row">
				<div class="col-md-6">
					<h3>Questions</h3>
					<ul class="list-group" id="dvSource">
						<?php if($questionData): ?>
							<?php $__currentLoopData = $questionData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $qt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li id="<?php echo e($qt->id); ?>" class="list-group-item"><?php echo e($qt->title); ?></li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php else: ?>
							No question found
						<?php endif; ?>
					</ul>
				</div>
			 <div class="col-md-6">
				<h3>Assign Questions</h3>
			 	<ul class="list-group" id="dvDest">
					<?php if($surveyQuestions): ?>
							<?php
							$questionArr=[];
							?>
							<?php $__currentLoopData = $surveyQuestions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $qt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li id="<?php echo e($qt->question->id); ?>" class="list-group-item"><?php echo e($qt->question->title); ?></li>
							<?php
							array_push($questionArr,$qt->question->id);
							?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php else: ?>
							No question found
						<?php endif; ?>
				</ul>
			 </div>
			  </div>
			  <input type="hidden" id="hiddenQuestionIds" name="hiddenQuestionIds" value="<?php echo e(implode(',',$questionArr)); ?>"/>
			  <button type="submit" class="btn btn-primary">Submit</button>
			</form> 
		</div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('myjsfile'); ?>
	<script>
	var questionArr=$('#hiddenQuestionIds').val().split(',');
	$( function() {
    	$("#dvSource li").draggable({
        revert: "invalid",
        refreshPositions: true,
        drag: function (event, ui) {
            ui.helper.addClass("draggable");
        },
        stop: function (event, ui) {
            ui.helper.removeClass("draggable");
        }
    });
    $("#dvDest").droppable({
        drop: function (event, ui) {
			
            if ($("#dvDest li").length == 0) {
                $("#dvDest").html("");
            }
		var qid=ui.draggable.attr('id');
		questionArr.push(qid);
		var questions=questionArr.toString();
		$('#hiddenQuestionIds').val(questions);
		ui.draggable.css({'left':'0','top':'5px'});
            $("#dvDest").append(ui.draggable);
        }
    });
	
	$("#dvDest li").draggable({
        revert: "invalid",
        refreshPositions: true,
        drag: function (event, ui) {
            ui.helper.addClass("draggable");
        },
        stop: function (event, ui) {
            ui.helper.removeClass("draggable");
        }
    });
    $("#dvSource").droppable({
        drop: function (event, ui) {
			
            if ($("#dvSource li").length == 0) {
                $("#dvSource").html("");
            }
		
		ui.draggable.css({'left':'0','top':'5px'});
		var qid=ui.draggable.attr('id');
		var index = questionArr.indexOf(qid);
		if (index > -1) {
		  questionArr.splice(index, 1);
		}
		$('#hiddenQuestionIds').val(questionArr.toString());
            $("#dvSource").append(ui.draggable);
        }
    });
  	});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app-after-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>