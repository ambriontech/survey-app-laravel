

<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">
			Survey Listing
		</h1>
		<a href="<?php echo e(route('surveyAdd')); ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> Add</a>
		<ol class="breadcrumb">
			<li class="active">
				<i class="fa fa-dashboard"></i> Survey Listing
			</li>
			
		</ol>
	</div>
</div>
    <div class="row justify-content-center">
        <div class="col-md-12">
			 <table id="myTable" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php if($allSurveys): ?>
        		<?php $__currentLoopData = $allSurveys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($sr->id); ?></td>
                <td><?php echo e($sr->name); ?></td>
                <td><?php echo e($sr->created_at); ?></td>
                <td>
                	<a class="btn btn-primary" href="<?php echo e(route('surveyEdit',[$sr->id])); ?>"><i class="fa fa-edit"></i> Edit</a> &nbsp; 
                	<a class="btn btn-danger" href="javascript:void(0);" data-url="<?php echo e(route('deleteSurvey',[$sr->id])); ?>" onclick="deleteSurvey(this);"><i class="fa fa-trash"></i> Delete</a>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
		</div>
    </div>
	
<?php $__env->stopSection(); ?>
<?php $__env->startSection('myjsfile'); ?>
<script>
	$(document).ready(function() {
   		$('#myTable').DataTable();
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app-after-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>