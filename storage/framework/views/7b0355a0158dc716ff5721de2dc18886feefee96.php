<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="<?php echo e(asset('public/css/bootstrap.min.css')); ?>" rel="stylesheet">

  <link href="<?php echo e(asset('public/css/sb-admin.css')); ?>" rel="stylesheet">

  <link href="<?php echo e(asset('public/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">

  <link href="<?php echo e(asset('public/css/style.css')); ?>" rel="stylesheet">
  <style>
        @page  {
            margin-top: 10pt;
        }
  </style>
</head>
<body style="margin-top:0;">
  <center><img class="img-responsive" src="<?php echo e(URL::to('/')); ?>/public/images/logo.png" alt="" style="width: 20%;"/></center>
  <h1 style="text-align:center;">Summary Report</h1>
  <div class="container-fluid">
   <div class="table-responsive">
  <table class="table" style="border:1px solid black;border-collapse: collapse;">
		<tr>
			<th width="50" class="text-center">Question</th>
			<th width="50" class="text-center">Answer</th>
		</tr>
		<?php if($finalArray): ?>
			<?php $i=0; ?>
			<?php $__currentLoopData = $finalArray; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($data->type=='t' || $data->type=='r'): ?>
					<tr <?php if($i>0): ?> style="border: 1px solid #000;border-bottom:none;border-right:none;border-left:none;" <?php endif; ?>>
						<td class="col-md-offset-1" style="padding:15px;"><b><?php echo e($data->title); ?></b></td>
						<td class="col-md-offset-1" style="padding:15px;"><?php echo e($data->answer); ?>

					</tr>
				<?php elseif($data->type=='sc'): ?>
					<tr style="border: 1px solid #000;border-bottom:none;border-right:none;border-left:none;"><td class="col-md-offset-1" style="padding:15px;padding-top:5px;"><b><?php echo e($data->title); ?></b></td><td></td></tr>
						<?php $__currentLoopData = $data->options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$opt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td class="col-md-offset-1" style="padding:15px;padding-top:0px;"><b><?php echo e($opt['value']); ?></b></td>
								<td class="col-md-offset-1" style="padding:15px;padding-top:0px;">
									<?php if($opt['answer']==$opt['value']): ?>
										<img class="img-responsive" src="<?php echo e(URL::to('/')); ?>/public/images/radio-checked.png" alt=""/>
									<?php else: ?>
										<img class="img-responsive" src="<?php echo e(URL::to('/')); ?>/public/images/radio-uncheck.png" alt=""/>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				<?php elseif($data->type=='mc'): ?>
					<tr style="border: 1px solid #000;border-bottom:none;border-right:none;border-left:none;"><td class="col-md-offset-1" style="padding:15px;padding-top:5px;"><b><?php echo e($data->title); ?></b></td><td></td></tr>
						<?php $__currentLoopData = $data->options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$opt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td class="col-md-offset-1" style="padding:15px;padding-top:0px;"><b><?php echo e($opt['value']); ?></b></td>
								<td class="col-md-offset-1" style="padding:15px;padding-top:0px;">
									<?php if($opt['answer']): ?>
										<img class="img-responsive" src="<?php echo e(URL::to('/')); ?>/public/images/tick.png" alt=""/>
									<?php else: ?>
										<img class="img-responsive" src="<?php echo e(URL::to('/')); ?>/public/images/cross.png" alt=""/>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					
				<?php endif; ?>
				<?php $i++ ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<?php endif; ?>
  </table>
</div>
  
</div>
</body>

</html>