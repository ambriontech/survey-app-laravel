$(document).ready(function(){
	shownSelectedType($('#questionTypeId').val());
	function shownSelectedType(value){
		$('.questionChoiceOptions').hide();
		$('input[name="options[]"]').prop('disabled',true);
		$('textarea[name="content_question"]').prop('disabled',true);
		switch(value){
			case'mc':
			$('#multipleChoiceOptions').show();
			$('#multipleChoiceOptions input[name="options[]').prop('disabled',false);
			break;
			case'sc':
			$('#SingleChoiceOptions').show();
			$('#SingleChoiceOptions input[name="options[]').prop('disabled',false);
			break;
			case'r':
			$('#rangeChoiceOptions').show();
			$('#rangeChoiceOptions input[name="options[]').prop('disabled',false);
			break;
			case 'cq':
			$('textarea[name="content_question"]').prop('disabled',false);
			$('#contentQuestionOptions').show();
			break;
		}
	}

		$("#question_add_form").validate({
			rules:{
		  title: "required",
		  type: "required",
		  'options[]': {
			required: true,
		  }
		  
	  },
	  messages:{
		  title : "This Field is Required!",
		  type : "This Field is Required!",
		  'options[]': {
			required: "This Field is Required!"
		  }
		  
	  }
	});


	$("#question_edit_form").validate({
				rules:{
			  title: "required",
			  type: "required",
			  'options[]': {
				required: true,
			  }
			  
		  },
		  messages:{
			  title : "This Field is Required!",
			  type : "This Field is Required!",
			  'options[]': {
				required: "This Field is Required!"
			  }
		  }
	});

	$("#survey_add_form").validate({
				rules:{
			  name: "required",
			  
		  },
		  messages:{
			  name : "This Field is Required!",
			  
		  }
	});

	$("#survey_edit_form").validate({
				rules:{
			  name: "required",
			  
		  },
		  messages:{
			  name : "This Field is Required!",
			  
		  }
	});

});

	//add edit question dropdown
	function selectQuestionType(th){
		$('.questionChoiceOptions').hide();
		$('input[name="options[]"]').prop('disabled',true);
		$('textarea[name="content_question"]').prop('disabled',true);
		switch($(th).val()){
			case'mc':
			$('#multipleChoiceOptions').show();
			$('#multipleChoiceOptions input[name="options[]').prop('disabled',false);
			break;
			case'sc':
			$('#SingleChoiceOptions').show();
			$('#SingleChoiceOptions input[name="options[]').prop('disabled',false);
			break;
			case'r':
			$('#rangeChoiceOptions').show();
			$('#rangeChoiceOptions input[name="options[]').prop('disabled',false);
			break;
			case 'cq':
			$('textarea[name="content_question"]').prop('disabled',false);
			$('#contentQuestionOptions').show();
			break;
		}
	}

	//delete question
	function deleteQuestion(th){
		if(confirm('Do you want to delete?')){
			window.location.href=$(th).data('url');
		}
	}
	
	//delete survey
	function deleteSurvey(th){
		if(confirm('Do you want to delete?')){
			window.location.href=$(th).data('url');
		}
	}

	