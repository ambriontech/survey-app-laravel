<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    function questions(){
		return $this->hasMany('App\SurveyOption','survey_id');
	}
}
