<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SurveySubmission;
use App\SurveyOption;
use App\QuestionOption;
use App\User;
use App\Question;
use PDF;
use Mail;

class SurveySubmissionController extends Controller{

	//add all questions to submission table at 1st question on add.
	private function addQuestions($sid,$sno,$uid){
		$questions=SurveyOption::where(array('survey_id'=>$sid))->get();
		if($questions){
			foreach ($questions as $qt) {
				$survey = new SurveySubmission;
				$survey->survey_id = $sid;
				$survey->question_id = $qt->question_id;
				$survey->user_id = $uid;
				$survey->submission_no = $sno;
				$survey->save();
			}
		}
	}

	//submit survey to table
    public function SubmitSurvey(Request $request,$sid,$uid,$sno){
		$postedArr=$request->all();
		if(isset($postedArr[0]['delete'])){
			SurveySubmission::where(array('user_id'=>$uid,'survey_id'=>$sid,'submission_no'=>$sno,'question_id'=>$postedArr[0]['qid']))->delete();

		}

		
		$sno=($sno)?$sno:$postedArr[0]['submission_no'];
		$subQueExist=SurveySubmission::where(array('survey_id'=>$sid,'user_id'=>$uid,'submission_no'=>$sno))->count();

		if($postedArr){
			if(!$subQueExist){
				$this->addQuestions($sid,$sno,$uid);
			}
			
			$totalQuestions=SurveyOption::where('survey_id',$sid)->count();

			$questionsQuery=$this->getSurveyQuestions($sid);

			$lastInsertedId=0;

			SurveySubmission::where(array('user_id'=>$uid,'survey_id'=>$sid,'submission_no'=>$sno,'question_id'=>$postedArr[0]['qid']))->delete();

			if(!isset($postedArr[0]['value'])){
				$survey = new SurveySubmission;
				$survey->survey_id = $sid;
				$survey->question_id = $postedArr[0]['qid'];
				$survey->user_id = $uid;
				$survey->submission_no = $sno;
				$survey->is_submitted = 1;
				$survey->save();

				$lastInsertedId=$survey->id;
			}
			else if(is_array($postedArr[0]['value'])) {
				foreach($postedArr[0]['value'] as $key=>$post){
					
					$survey = new SurveySubmission;

					$survey->survey_id = $sid;

					$survey->question_id = $postedArr[0]['qid'];

					$survey->answer = $post;

					$survey->user_id = $uid;

					$survey->submission_no = $sno;
					$survey->is_submitted = 1;

					$survey->save();

					$lastInsertedId=$survey->id;
				}
			}else{
				$survey = new SurveySubmission;

				$survey->survey_id = $sid;

				$survey->question_id = $postedArr[0]['qid'];
				
				$survey->answer = $postedArr[0]['value'];

				$survey->user_id = $uid;

				$survey->submission_no = $sno;
				$survey->is_submitted = 1;
				$survey->save();

				$lastInsertedId=$survey->id;
			}
			
			if($questionsQuery[($questionsQuery->count())-1]->id==$postedArr[0]['qid']){
					$this->sendEmail($uid,$sno);
			}
			
			$totalAnswers=SurveySubmission::where(array('survey_id'=>$sid,'user_id'=>$uid,'submission_no'=>$sno,'is_submitted'=>1))->groupBy('question_id')->get();
			$totalAnswers=count($totalAnswers);
			$data['percentage']=0;
			if($lastInsertedId>0){
				
				if($totalAnswers>0){
					$data['percentage']=round(($totalAnswers*100)/$totalQuestions);
				}
				
				if($lastInsertedId>0){

				$data['type']="success";

				$data['message']="Survey submission successfully";

			}else{

				$data['type']="error";

				$data['message']="There is some problem in submission";

			}

			}

			return response()->json($data);
		}

    }

	

	public function userSubmissionListing($uid){

		if($uid){
			$surveyList=SurveySubmission::with(['survey'])->where('user_id',$uid)->groupBy('submission_no')->get()->toArray();
			//echo "<pre>";print_r($surveyList);die;
			if($surveyList){
				$i=0;
				foreach ($surveyList as $survey) {					
				$surveyList[$i]['answer']=SurveySubmission::where(array('submission_no'=>$survey['submission_no'],'is_submitted'=>1,'question_id'=>1,'user_id'=>$survey['user_id']))->whereNotNull('answer')->first()->answer;
					$totalQuestions=SurveyOption::where('survey_id',$survey['survey_id'])->count();

					$totalAnswers=SurveySubmission::where(array('survey_id'=>$survey['survey_id'],'user_id'=>$uid,'submission_no'=>$survey['submission_no'],'is_submitted'=>1))->groupBy('question_id')->get();
					$surveyList[$i]['percentage']=0;
					if($totalAnswers){
						$surveyList[$i]['percentage']=
						round((count($totalAnswers)*100)/$totalQuestions);
					}
					$surveyList[$i]['last_question']=($surveyList[$i]['percentage']!=100)?count($totalAnswers):0;
					$i++;
				}
			}
			return $surveyList;
		}

	}

	public function userSubmissionCount($uid){

		if($uid){
			$surveyList=SurveySubmission::with(['survey'])->where('user_id',$uid)->groupBy('submission_no')->get()->toArray();
			if($surveyList){
				$length=count($surveyList);
				return $surveyList[$length-1]['submission_no'];
			}else{
				return 0;
			}
		}

	}

	private function getSurveyQuestions($sid){
		return $submittedSurveys=\DB::table('survey_options')
			->join('questions', 'questions.id', '=', 'survey_options.question_id')
			->where('survey_options.survey_id',$sid)
			->select('questions.id')
			->get();
	}
	public function sendEmail($uid,$sno){
		/* $uid=2;
		$sno=1; */
		$submittedSurveys=\DB::table('survey_submissions')
			->join('surveys', 'surveys.id', '=', 'survey_submissions.survey_id')
			->join('questions', 'questions.id', '=', 'survey_submissions.question_id')
			->select('surveys.name','surveys.created_at','surveys.updated_at','questions.*','survey_submissions.question_id','survey_submissions.user_id','survey_submissions.survey_id','survey_submissions.answer')
			->where([['survey_submissions.user_id','=',$uid],['survey_submissions.submission_no','=',$sno],
			['questions.type','<>','cq']])
			->groupBy('survey_submissions.question_id')
			->get()->toArray();
		$finalArray=$submittedSurveys;

		if($finalArray){
			$i=0;

			foreach($submittedSurveys as $question){
				if($question->type!='t'){
					$finalArray[$i]->options=[];
					$options=QuestionOption::where('question_id',$question->id)->get()->toArray();
					$answerArr=SurveySubmission::where(array('question_id'=>$options[0]['question_id'],'user_id'=>$uid,'survey_submissions.submission_no'=>$sno))->get()->toArray();

					if($options){
						$j=0;
						foreach($options as $opt){
							$finalArray[$i]->options[$j]=$opt;

							$finalArray[$i]->options[$j]['answer']=(isset($answerArr[$j]))?$answerArr[$j]['answer']:$answerArr[0]['answer'];
							if($question->type=='sc' || $question->type=='mc'){
								if($question->type=='sc'){
									if(!$finalArray[$i]->options[$j]['answer']){
										$finalArray[$i]->options[$j]['answer']=$options[0]['value'];
									}
								}
								$finalArray[$i]->options[$j]['checked']=($finalArray[$i]->options[$j]['answer']=='1' || $finalArray[$i]->options[$j]['value']==$finalArray[$i]->answer)?true:false;
							}
							$j++;

						}

					}

				}
				$i++;
			}

		}
		$data['finalArray']=$finalArray;
			//echo "<pre>";print_r($finalArray);die;
			$pdf = PDF::loadView('emails.surveySubmissions', $data);
			$filename='dcoument_'.time().'.pdf';
			$pdfFilePath=storage_path('app/public/survey/'.$filename);
			$pdf->save($pdfFilePath);

			//return $pdf->stream($filename);
			//return view('emails.surveySubmissions',compact('finalArray'));
			$user=User::find($uid)->toArray();
			$user['pdfFilePath']=$pdfFilePath;
			if(file_exists($pdfFilePath)){
				Mail::send('emails.surveyEmail', ['user' => $user], function ($m) use ($user) {
        			$m->to($user['email'])->subject('Life Support Decision Aid Results');
					$m->attach($user['pdfFilePath']);
    			});
			}
			
	}

}

