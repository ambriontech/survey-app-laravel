<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Question;

use App\QuestionOption;

use Illuminate\Support\Facades\Session;

class QuestionController extends Controller{

	function listing(){

		$allQuestions=Question::all();

		return view('question.index')->with(compact('allQuestions'));

	}

    function addQuestion(Request $request){
    	//echo "<pre>";print_r($request->all());die;

		$question=new Question;

		if ($request->isMethod('post')) {

			$postedArr=$request->all();
			if(isset($postedArr['title']))
				$question->title=$postedArr['title'];

			$question->type=$postedArr['type'];

			$question->subtitle=$postedArr['subtitle'];

			if(isset($postedArr['content_question']))
				$question->content=$postedArr['content_question'];

			$question->save();

			if(isset($postedArr['options']) && $question->id>0){

				foreach($postedArr['options'] as $option){

					$questionOption=new QuestionOption;

					$questionOption->question_id=$question->id;

					$questionOption->value=$option;

					$questionOption->save();

				}

			}

			Session::flash('success', 'Question added successfully.');

			return redirect()->route('questionListing');

		}

		return view('question.add');

	}

	function editQuestion($id,Request $request){

		if($id){

			$questionData=Question::with(['options'])->find($id);

			if ($request->isMethod('post')) {

				$postedArr=$request->all();

				if(isset($postedArr['title']))
					$questionData->title=$postedArr['title'];

				$questionData->type=$postedArr['type'];

				$questionData->subtitle=$postedArr['subtitle'];

				if(isset($postedArr['content_question']))
					$questionData->content=$postedArr['content_question'];

				$questionData->save();

				if(isset($postedArr['options'])){

					QuestionOption::where('question_id',$id)->delete();
					foreach($postedArr['options'] as $option){

						$questionOption=new QuestionOption;

						$questionOption->question_id=$id;

						$questionOption->value=$option;

						$questionOption->save();

					}

				}

				Session::flash('success', 'Question updated successfully.');

				return redirect()->route('questionListing');

			}

		}

		return view('question.edit')->with(compact('questionData'));

	}



	function deleteQuestion($id){

		if($id){

			Question::find($id)->delete();

			Session::flash('success', 'Question deleted successfully.');

			return redirect()->route('questionListing');

		}

	}

}

