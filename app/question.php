<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model{
    function options(){
		return $this->hasMany('App\QuestionOption','question_id');
	}
	function surveys(){
		return $this->hasMany('App\SurveyOption','question_id');
	}
}
