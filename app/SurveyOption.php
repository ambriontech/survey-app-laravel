<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyOption extends Model
{
    function survey(){
		return $this->belongsTo('App\Survey');
	}
	
	function question(){
		return $this->belongsTo('App\Question');
	}
}
