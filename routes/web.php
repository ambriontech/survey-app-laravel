<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function () {
	Route::get('/questions', 'QuestionController@listing')->name('questionListing');
	Route::get('/add-question', 'QuestionController@addQuestion')->name('questionAdd');
	Route::post('/add-question', 'QuestionController@addQuestion')->name('questionSaved');
	Route::get('/edit-question/{id}', 'QuestionController@editQuestion')->name('questionEdit');
	Route::post('/edit-question/{id}', 'QuestionController@editQuestion')->name('questionUpdate');
	Route::get('/delete-question/{id}', 'QuestionController@deleteQuestion')->name('deleteQuestion');
	
	Route::get('/add-survey', 'SurveyController@addSurvey')->name('surveyAdd');
	Route::post('/add-survey', 'SurveyController@addSurvey')->name('surveySaved');
	Route::get('/edit-survey/{id}', 'SurveyController@editSurvey')->name('surveyEdit');
	Route::post('/edit-survey/{id}', 'SurveyController@editSurvey')->name('surveyUpdate');
	Route::get('/surveys', 'SurveyController@listing')->name('surveyListing');
	Route::get('/delete-survey/{id}', 'SurveyController@deleteSurvey')->name('deleteSurvey');
});

