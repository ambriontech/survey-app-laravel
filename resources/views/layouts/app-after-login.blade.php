<!DOCTYPE html>

<html lang="en">



<head>



    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Bootstrap Core CSS -->

    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">

	<link href="{{ asset('public/css/sb-admin.css') }}" rel="stylesheet">

	<link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

	<link href="{{ asset('public/css/style.css') }}" rel="stylesheet">

	<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

	<!-- Fonts -->

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

</head>



<body>

	<div id="wrapper">

		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>

                <a class="navbar-brand" href="index.html">Survey Admin</a>

            </div>

            @include('includes.top-menu')

            <div class="collapse navbar-collapse navbar-ex1-collapse">

                @include('includes.sidebar')

            </div>

        </nav>

		<div id="page-wrapper">

		

			@if(Session::has('success'))

				<div class="alert alert-success">

					<strong>Success!</strong> {{Session::get('success')}}

				</div>

			@endif

			@if(Session::has('warning'))

			<div class="alert alert-danger">

				<strong>Warning!</strong> {{Session::get('warning')}}

			</div>

			@endif

			

			<div class="container-fluid">

				@yield('content')

            </div>

            

		</div>

    </div>

    

	<!--scripts-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="{{ asset('public/js/validate.min.js') }}"></script>
    
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

	 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>

	@yield('myjsfile')

	<script src="{{ asset('public/js/common.js') }}"></script>

</body>



</html>

