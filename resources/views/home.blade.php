@extends('layouts.app-after-login')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">
			Dashboard
		</h1>
		<ol class="breadcrumb">
			<li class="active">
				<i class="fa fa-dashboard"></i> Dashboard
			</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-question-circle fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge">{{$questionCount}}</div>
						<div>Total Questions!</div>
					</div>
				</div>
			</div>
			<a href="{{route('questionListing')}}">
				<div class="panel-footer">
					<span class="pull-left">View Details</span>
					<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-green">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="fa fa-tasks fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge">{{$surveyCount}}</div>
						<div>Total Survey!</div>
					</div>
				</div>
			</div>
			<a href="{{route('surveyListing')}}">
				<div class="panel-footer">
					<span class="pull-left">View Details</span>
					<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
</div>
@endsection
