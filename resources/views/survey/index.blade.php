@extends('layouts.app-after-login')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">
			Survey Listing
		</h1>
		<a href="{{route('surveyAdd')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> Add</a>
		<ol class="breadcrumb">
			<li class="active">
				<i class="fa fa-dashboard"></i> Survey Listing
			</li>
			
		</ol>
	</div>
</div>
    <div class="row justify-content-center">
        <div class="col-md-12">
			 <table id="myTable" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        	@if($allSurveys)
        		@foreach($allSurveys as $sr)
            <tr>
                <td>{{$sr->id}}</td>
                <td>{{$sr->name}}</td>
                <td>{{$sr->created_at}}</td>
                <td>
                	<a class="btn btn-primary" href="{{route('surveyEdit',[$sr->id])}}"><i class="fa fa-edit"></i> Edit</a> &nbsp; 
                	<a class="btn btn-danger" href="javascript:void(0);" data-url="{{route('deleteSurvey',[$sr->id])}}" onclick="deleteSurvey(this);"><i class="fa fa-trash"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
		</div>
    </div>
	
@endsection
@section('myjsfile')
<script>
	$(document).ready(function() {
   		$('#myTable').DataTable();
	});
</script>
@endsection
