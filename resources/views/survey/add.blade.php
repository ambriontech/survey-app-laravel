@extends('layouts.app-after-login')



@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-8">

			 <h2 class="text-center">Add Survey</h2>

			 <form action="{{route('surveySaved')}}" method="post" id="survey_add_form">

			  @csrf

			  <div class="form-group">

				<label for="name">Name:</label>

				<input type="text" class="form-control" name="name" id="name">

			  </div>

			  <div class="row">

				<div class="col-md-6">

					<h3>Questions</h3>

					<ul class="list-group" id="dvSource">

						@if($questionData)

							@foreach($questionData as $qt)

								<li id="{{$qt->id}}" class="list-group-item">{{$qt->title}}</li>

							@endforeach

						@else

							No question found

						@endif

					</ul>

				</div>

			 <div class="col-md-6">

				<h3>Assign Questions</h3>

			 	<ul class="list-group" id="dvDest">

					<center><b>Drop Here</b></center>

				</ul>

			 </div>

			  </div>

			  <input type="hidden" id="hiddenQuestionIds" name="hiddenQuestionIds" value=""/>

			  <button type="submit" class="btn btn-primary">Submit</button>

			</form> 

		</div>

    </div>

</div>

@endsection

@section('myjsfile')

	<script>

	var questionArr=[];

	$( function() {

    	$("#dvSource li").draggable({

        revert: "invalid",

        refreshPositions: true,

        drag: function (event, ui) {

            ui.helper.addClass("draggable");

        },

        stop: function (event, ui) {

            ui.helper.removeClass("draggable");

        }

    });

    $("#dvDest").droppable({

        drop: function (event, ui) {

			

            if ($("#dvDest li").length == 0) {

                $("#dvDest").html("");

            }

		var qid=ui.draggable.attr('id');

		questionArr.push(qid);

		var questions=questionArr.toString();

		$('#hiddenQuestionIds').val(questions);

		ui.draggable.css({'left':'0','top':'5px'});

            $("#dvDest").append(ui.draggable);

        }

    });

	$("#dvDest li").draggable({

        revert: "invalid",

        refreshPositions: true,

        drag: function (event, ui) {

            ui.helper.addClass("draggable");

        },

        stop: function (event, ui) {

            ui.helper.removeClass("draggable");

        }

    });

    $("#dvSource").droppable({

        drop: function (event, ui) {

			

            if ($("#dvSource li").length == 0) {

                $("#dvSource").html("");

            }

		

		ui.draggable.css({'left':'0','top':'5px'});

		var qid=ui.draggable.attr('id');

		var index = questionArr.indexOf(qid);

		if (index > -1) {

		  questionArr.splice(index, 1);

		}

		$('#hiddenQuestionIds').val(questionArr.toString());

            $("#dvSource").append(ui.draggable);

        }

    });

  	});

	</script>

@endsection