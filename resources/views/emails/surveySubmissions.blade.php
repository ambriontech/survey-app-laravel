<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">

  <link href="{{ asset('public/css/sb-admin.css') }}" rel="stylesheet">

  <link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

  <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
  <style>
        @page {
            margin-top: 10pt;
        }
  </style>
</head>
<body style="margin-top:0;">
  <center><img class="img-responsive" src="{{URL::to('/')}}/public/images/logo.png" alt="" style="width: 20%;"/></center>
  <h1 style="text-align:center;">Summary Report</h1>
  <div class="container-fluid">
   <div class="table-responsive">
  <table class="table" style="border:1px solid black;border-collapse: collapse;">
		<tr>
			<th width="50" class="text-center">Question</th>
			<th width="50" class="text-center">Answer</th>
		</tr>
		@if($finalArray)
			@php $i=0; @endphp
			@foreach($finalArray as $data)
				@if($data->type=='t' || $data->type=='r')
					<tr @if($i>0) style="border: 1px solid #000;border-bottom:none;border-right:none;border-left:none;" @endif>
						<td class="col-md-offset-1" style="padding:15px;"><b>{{$data->title}}</b></td>
						<td class="col-md-offset-1" style="padding:15px;">{{$data->answer}}
					</tr>
				@elseif($data->type=='sc')
					<tr style="border: 1px solid #000;border-bottom:none;border-right:none;border-left:none;"><td class="col-md-offset-1" style="padding:15px;padding-top:5px;"><b>{{$data->title}}</b></td><td></td></tr>
						@foreach($data->options as $key=>$opt)
							<tr>
								<td class="col-md-offset-1" style="padding:15px;padding-top:0px;"><b>{{$opt['value']}}</b></td>
								<td class="col-md-offset-1" style="padding:15px;padding-top:0px;">
									@if($opt['answer']==$opt['value'])
										<img class="img-responsive" src="{{URL::to('/')}}/public/images/radio-checked.png" alt=""/>
									@else
										<img class="img-responsive" src="{{URL::to('/')}}/public/images/radio-uncheck.png" alt=""/>
									@endif
								</td>
							</tr>
						@endforeach

				@elseif($data->type=='mc')
					<tr style="border: 1px solid #000;border-bottom:none;border-right:none;border-left:none;"><td class="col-md-offset-1" style="padding:15px;padding-top:5px;"><b>{{$data->title}}</b></td><td></td></tr>
						@foreach($data->options as $key=>$opt)
							<tr>
								<td class="col-md-offset-1" style="padding:15px;padding-top:0px;"><b>{{$opt['value']}}</b></td>
								<td class="col-md-offset-1" style="padding:15px;padding-top:0px;">
									@if($opt['answer'])
										<img class="img-responsive" src="{{URL::to('/')}}/public/images/tick.png" alt=""/>
									@else
										<img class="img-responsive" src="{{URL::to('/')}}/public/images/cross.png" alt=""/>
									@endif
								</td>
							</tr>
						@endforeach
					
				@endif
				@php $i++ @endphp
			@endforeach
		@endif
  </table>
</div>
  
</div>
</body>

</html>