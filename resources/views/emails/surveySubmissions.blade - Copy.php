<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">

  <link href="{{ asset('public/css/sb-admin.css') }}" rel="stylesheet">

  <link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

  <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
</head>
<body>
  <h1 style="text-align:center;">Summary Report</h1>
  <div class="container-fluid" style="border:1px solid black">
   <div class="table-responsive">
  <table class="table" border="1">
		<tr>
			<th width="50">Questions</th>
			<th width="50">Answers</th>
		</tr>
		@if($finalArray)
			@foreach($finalArray as $data)
				<tr>
					<td>
						<b>{{$data->title}}</b>
						@if($data->type=='mc' || $data->type=='sc')
							<table>
							<tbody>
							@foreach($data->options as $opt)
								<tr>
									<td>{{$opt['value']}}</td>
								</tr>
							@endforeach
						  	</tbody>
						  	</table>
						 @endif
					</td>
					<td>
						<table>
							<tbody>
						@if($data->type=='mc')
						
							@foreach($data->options as $key=>$opt)
								<tr>
									<td>
										@if($key==0) <br/>@endif
										
									@if($opt['answer'])
										<img class="img-responsive" src="{{URL::to('/')}}/public/images/tick.png" alt=""/>
									@else
										<img class="img-responsive" src="{{URL::to('/')}}/public/images/cross.png" alt=""/>
									@endif
									</td>
								</tr>
							@endforeach
							@elseif($data->type=='sc')
						  
							@foreach($data->options as $key=>$opt)
								<tr>
									<td>
										@if($key==0) <br/>@endif
								@if($opt['answer']==$opt['value'])
									<img class="img-responsive" src="{{URL::to('/')}}/public/images/radio-checked.png" alt=""/>
								@else
									<img class="img-responsive" src="{{URL::to('/')}}/public/images/radio-uncheck.png" alt=""/>
								@endif
						 	</td>
								</tr>
							@endforeach
						  @else
						  	<tr><td>{{$data->answer}}</td></tr>
						  @endif
          				</tbody>
          			</table>
					</td>
				</tr>
			@endforeach
		@endif
  </table>
</div>
  
</div>
</body>

</html>